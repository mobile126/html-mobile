
function workshop1(x,y) {
    let sum = (x+y)*(y+x);
    return "("+x+"+"+y+")*("+y+"+"+x+")="+sum;
}

function workshop2(n) {
    var sum = 0;
    for(var i=1; i<=n; i++){
        sum += i;
    }
    return sum;
}

function workshop3(stu) {
    let str = "";
    for(var i=0; i<=stu.length; i++){
        str += stu[i];
    }
    return str;
}

function main() {
    result = workshop1(4,3);
    console.log(result);
    num = workshop2(4);
    console.log(num);
    stu = workshop3("Mobile Paradigm")
    console.log(stu)
}

main()
