const express = require('express')
var cors = require('cors')
var path = require('path')
var bodyParser = require('body-parser')

const app = express()
const port = 3000
imageUrl = `http://localhost:${port}/images`

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

catsData = [
    {'name': 'the Kook', 'image': `${imageUrl}/1.jpg`,},
    {'name': 'The Chosen One.', 'image': `${imageUrl}/2.jpeg`,},
    {'name': 'The Drunk', 'image': `${imageUrl}/3.jpeg`,},
    {'name': 'The Whhhhh.......', 'image': `${imageUrl}/4.jpg`,},
    {'name': 'The angrily Cat', 'image': `${imageUrl}/5.jpeg`,},
    {'name': 'Who ?', 'image': `${imageUrl}/6.jpeg`, },
    {'name': 'The Protector', 'image': `${imageUrl}/7.jpg`,},
    {'name': 'The Cute Cat', 'image': `${imageUrl}/8.jpg`,},
    {'name': 'The Earth Aspect', 'image': `${imageUrl}/cat-b1.png`,},
    {'name': 'The Fire Aspect', 'image': `${imageUrl}/cat-b2.png`,},
    {'name': 'The Water Aspect', 'image': `${imageUrl}/cat-b3.png`, },
    {'name': 'The Wind Aspect', 'image': `${imageUrl}/cat-b4.jpg`,}
];

dogsData = [
    {'name': 'the Mongo', 'image': `${imageUrl}/dog1.jpg`,},
    {'name': 'Dummmm', 'image': `${imageUrl}/dog2.jpeg`,},
    {'name': 'Amazonnnn', 'image': `${imageUrl}/dog3.jpg`,},
    {'name': 'Whatt?', 'image': `${imageUrl}/dog4.jpeg`,},
    {'name': 'The angrily Dog', 'image': `${imageUrl}/dog5.jpg`,},
    {'name': 'Who ?', 'image': `${imageUrl}/catOntheChair.png`, },
    // {'name': 'The Protector', 'image': `${imageUrl}/7.jpg`,},
    // {'name': 'The Cute Cat', 'image': `${imageUrl}/8.jpg`,},
    // {'name': 'The Earth Aspect', 'image': `${imageUrl}/cat-b1.png`,},
    // {'name': 'The Fire Aspect', 'image': `${imageUrl}/cat-b2.png`,},
    // {'name': 'The Water Aspect', 'image': `${imageUrl}/cat-b3.png`, },
    // {'name': 'The Wind Aspect', 'image': `${imageUrl}/cat-b4.jpg`,}
]

let walletData = [
    {
        name: "โค้ก",
        price: 17
    },
    {
        name: "คุกกี้อาร์เซนอล",
        price: 71,
    },
    {
        name: "หมูกระทะ",
        price: 200
    },
]

var dir = path.join(__dirname, 'images');
app.use('/images', express.static(dir));

app.get('/cats', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    res.json(catsData);
})

app.get('/dogs', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    res.json(dogsData);
});


// homework 2
app.get('/wallet', (req, res) => {
    // search by name
    let name = req.query['name'];
    if(name === undefined) {
        // return all
        res.json(walletData);
    } else {
        let filtedData = walletData.filter((value) => value.name.indexOf(name) >= 0 ? true : false);
        res.json(filtedData);
    }
});

app.post('/wallet', (req, res) => {
    let d = req.body;
    if(typeof(d) !== "object") {
        res.status(400); // Bad Request
        res.json({'data': 'no has data'});
    } else if(d.name === undefined) {
        res.status(400); // Bad Request
        res.json({'data': 'no name field'});
    } else if (d.price === undefined) {
        res.status(400); // Bad Request
        res.json({'data': 'no price field'});
    } else {
        walletData.push(d);
        res.status(200);
        res.json({'data': 'success'})
    }
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})