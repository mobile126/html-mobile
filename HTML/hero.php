<?php
interface Superhero
{
    public function  UseWeapon();
    public function  Move();
}
abstract class Spider
{
    protected $numOfLegs;
    public function __construct($legs = "")
    {
        $this->numOfLegs;
    }
    protected function Climb()
    {
        printf("climb on wall");
    }
}
class Ironman implements Superhero
{
    private $weapon;
    public function __construct($weapon = " ")
    {
        $this->weapon;
    }
    public function UseWeapon()
    {
        printf("use %s to attack monster. ",$this->weapon);
    }
    public function Move()
    {
        printf("fly withengine\n");
    }
}
class Spiderman extends Spider implements Superhero
{
    private $weapon;
    public function __construct($weapon = " ")
    {
        parent::__construct(2);
        $this->weapon = $weapon;
    }
    public function UseWeapon()
    {
        printf("use %s to attack monster. ",$this->weapon);
    }
    public function Move()
    {
        printf("walk on %d legs\n",$this->numOfLegs);
    }
}
$ironman = new Ironman("Gun");
$spiderman = new Spiderman("Web");
printf("Ironman ");
$ironman->UseWeapon();
printf(" And ");
$ironman->Move();
printf("Spiderman ");
$spiderman->UseWeapon();
printf(" And ");
$spiderman->Move();
?>