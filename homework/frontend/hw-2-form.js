//
// ตัวอย่างโค้ดส่งไปยัง Server โดยใช้ Ajax Post
//

function sendData() {
    // เช็คมีข้อมูลหรือเปล่า
    let createDataRequest = new XMLHttpRequest();

    createDataRequest.open("POST", "http://localhost:3000/wallet", true);
    createDataRequest.setRequestHeader("Content-type", "application/json");

    createDataRequest.send(
        JSON.stringify({
        name: "กระดาษรายงาน", 
        price: 20
    }));
}

