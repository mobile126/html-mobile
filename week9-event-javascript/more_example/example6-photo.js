function createImage(src) {
  const image = document.createElement("img");
  image.src = src;
  return image;
}

function onThumbnailClick(event) {
    //
}

function onModalClick() {
  hideModal();
}

function hideModal() {
  document.body.classList.remove("no-scroll");
  modalView.classList.add("hidden");
  modalView.innerHTML = "";
  document.removeEventListener("keydown", nextPhoto);
}

function nextPhoto(event) {
  if (event.key === "Escape") {
     //
    return;
  }

  if (event.key !== "ArrowLeft" && event.key !== "ArrowRight") {
    return;
  }

  //
}

let currentIndex = null;
const albumView = document.querySelector("#album-view");
for (let i = 0; i < PHOTO_LIST.length; i++) {
  const photoSrc = PHOTO_LIST[i];
  const image = createImage(photoSrc);
  image.dataset.index = i;
  image.addEventListener("click", onThumbnailClick);
  albumView.appendChild(image);
}

const modalView = document.querySelector("#modal-view");
modalView.addEventListener("click", onModalClick);
