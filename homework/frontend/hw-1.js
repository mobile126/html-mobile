const uri = "http://localhost:3000"
var currentData = null;
var showList = document.querySelector('#showList');

function renderView() {
    console.log(currentData);
    showList.innerHTML = "";
    
    for (let index = 0; index < currentData.length; index++) {
        let divCol = document.createElement('div');
        divCol.classList.add('col-lg-4');

        let mainCard = document.createElement('div');
        mainCard.classList.add('card');
        mainCard.classList.add('mb-1');

        let imgCard = document.createElement('img');
        imgCard.classList.add('card-img-top');
        imgCard.src = this.currentData[index].image;
        // imgCard.width = 250;
        imgCard.height = 250;

        bodyCard = document.createElement('div');
        bodyCard.classList.add('card-body');
        
        headerEle = document.createElement('h5');
        headerEle.classList.add('card-title');
        headerEle.innerHTML = this.currentData[index].name;

        bodyCard.appendChild(headerEle);
        mainCard.appendChild(bodyCard);

        mainCard.appendChild(imgCard);
        mainCard.appendChild(bodyCard);

        divCol.appendChild(mainCard);
        showList.appendChild(divCol);
    }
}

function getCatData() {
    let request = fetch(`${uri}/cats`);
    request.then((response) => {
        return response.json();
    }).then((response) => {
        currentData = response;
        renderView();
    });
}

function getDogData() {
    let request = fetch(`${uri}/dogs`);
    request.then((response) => {
        return response.json();
    }).then((response) => {
        currentData = response;
        renderView();
    });
}

const loadCat = document.querySelector('#getCats');
loadCat.addEventListener('click', getCatData);

const loadDog = document.querySelector('#getDogs');
loadDog.addEventListener('click', getDogData);

